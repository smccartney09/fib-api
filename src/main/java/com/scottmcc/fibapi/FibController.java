package com.scottmcc.fibapi;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.scottmcc.fibapi.models.FibRequest;

@RestController
public class FibController {

  @CrossOrigin
  @GetMapping(value = "/health", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> getHealth() {
    return ResponseEntity.ok("{ \"status\": \"UP\" }");
  }

  @CrossOrigin
  @PostMapping(value = "/genFibNumber", consumes = MediaType.APPLICATION_JSON_VALUE, 
    produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Long> getNextFibNumber(@RequestBody FibRequest request) {
    return ResponseEntity.ok(this.getNextFibNumber(request.getNumber()));
  }

  private Long getNextFibNumber(Long i) {
    if (i < 0) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Input a positive integer");
    }
    Long a = 0L;
    Long b = 1L;
    while (b <= i) {
      Long temp = a;
      a = b;
      b = temp + b;
    }
    return b;
  }
}