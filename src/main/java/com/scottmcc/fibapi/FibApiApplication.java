package com.scottmcc.fibapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FibApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FibApiApplication.class, args);
	}

}
