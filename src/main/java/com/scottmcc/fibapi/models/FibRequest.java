package com.scottmcc.fibapi.models;

public class FibRequest {
  
  private Long number;

  public FibRequest() { this.number = 0L; }
  public FibRequest(Long number) { this.number = number; }
  
  public void setNumber(Long number) { this.number = number; }
  public Long getNumber() { return number; }
}
